package mqtt

import (
	"fmt"
	"log"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

//Callback function called each time that mosquitto receives messages.
type Callback func(isError bool, topic string, parameter string)

//CreateConnection mqtt
func CreateConnection(url string, clientID string, isDebug bool, cb Callback) mqtt.Client {
	if isDebug == true {
		mqtt.DEBUG = log.New(os.Stdout, "", 0)
		mqtt.ERROR = log.New(os.Stdout, "", 0)
	}

	var publish mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
		topic := msg.Topic()
		message := string(msg.Payload())
		fmt.Printf("TOPIC: %s\n", topic)
		fmt.Printf("MSG: %s\n", message)
		cb(false, msg.Topic(), message)
	}

	opts := mqtt.NewClientOptions().AddBroker(url).SetClientID(clientID)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetDefaultPublishHandler(publish)
	opts.SetPingTimeout(1 * time.Second)

	/* do connection */
	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return c
}

//Subscribe mqtt
func Subscribe(client mqtt.Client, topic string) {
	if token := client.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		// os.Exit(1)
	}
}

//Unsubscribe mqtt
func Unsubscribe(client mqtt.Client, topic string) {
	if token := client.Unsubscribe(topic); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
	}
}

//Publish mqtt
func Publish(client mqtt.Client, topic string, msg string) {
	token := client.Publish(topic, 0, false, msg)
	token.Wait()
}

//Disconnect mqtt
func Disconnect(client mqtt.Client) {
	client.Disconnect(250)
}

// func main() {

// 	var s = func(isError bool, topic string, msg string) {
// 		fmt.Println("From client " + topic + " " + msg)
// 	}
// 	c := CreateConnection("tcp://127.0.0.1:1883", "gotrivial", true, s)
// 	Subscribe(c, "hello")
// 	Publish(c, "hello", "hello world")
// 	time.Sleep(1 * time.Second)
// 	Disconnect(c)

// mqtt.DEBUG = log.New(os.Stdout, "", 0)
// mqtt.ERROR = log.New(os.Stdout, "", 0)

// opts := mqtt.NewClientOptions().AddBroker("tcp://127.0.0.1:1883").SetClientID("gotrivial")
// opts.SetKeepAlive(2 * time.Second)
// opts.SetDefaultPublishHandler(publish)
// opts.SetPingTimeout(1 * time.Second)

// /* do connection */
// c := mqtt.NewClient(opts)
// if token := c.Connect(); token.Wait() && token.Error() != nil {
// 	panic(token.Error())
// }

// const topic string = "go-mqtt/sample"

// /* subscriber */
// if token := c.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
// 	fmt.Println(token.Error())
// 	os.Exit(1)
// }

// /* publisher */
// for i := 0; i < 5; i++ {
// 	text := fmt.Sprintf("this is the message #%d!", i)
// 	token := c.Publish(topic, 0, false, text)
// 	token.Wait()
// }

// time.Sleep(6 * time.Second)

// if token := c.Unsubscribe(topic); token.Wait() && token.Error() != nil {
// 	fmt.Println(token.Error())
// 	os.Exit(1)
// }

// c.Disconnect(250)
// time.Sleep(1 * time.Second)
// }
